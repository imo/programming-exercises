# Ordnung im CD Regal

Du hast ein CD Regal mit 100 Plätzen. Schreib eine Konsolenanwendung um das Regal zu verwalten. Bearbeite dazu folgende Schritte:

* Neues Projekt anlegen
* Erstelle eine Klasse die einen Stellplatz repräsentiert
   * sie soll den die Belegung (belegt/nicht belegt) speichern, den Titel und Interpret
   * geeignete Datentypen sollen verwendet werden
   * soll getter und setter Methoden enthalten
   * eine Konstruktur bereit stellen
* Erstellen einer zweiten Klasse welche das Regal repräsentiert
   * Array soll die 100 Stellplätze enthalten und die Indexes soll die Stellplatznummern repräsentieren
   * Initialisierung des Arrays im Konstruktur
   * die Stellplatz Klasse soll dabei als neuer Datentyp verwendet werden
   * Benutzer Interface der Klasse soll dabei folgendermaßen aussehen (beispielhafte Namen):
      * CD_einstellen(titel, interpret, position)
      * CD_entfernen(position)
      * CD_suchen(titel[, interpret])
      * zeige_alle_CDs() - Ausgabeformat in Tabellenform
      * zeige_alle_CDs_vom_Interpret(intpret) - ebenfalls Tabellenform
* Erstellen einer dritten Klasse oder einer Funktion `Main`
   * z.B. ein kleines Menü welches die Funktionen ausgibt und woraus der Benutzer wählen kann
   * die Funktionen sollen dabei an die Regal Klasse angelehnt sein
   * ggf. um weitere Funktionen ergänzen
