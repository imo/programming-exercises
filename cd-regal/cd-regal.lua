#!/usr/bin/env lua
--
-- Interface:
-- - add titel interpret position
-- - del position
-- - search titel [interpret]
-- - show-all
-- - show-from-interpret
-- - q oder quit oder exit um zu beenden


-- Stellplatz Klasse
local ShelfPlace = {}

-- Konstruktur
function ShelfPlace.new()
    local self = {
        in_use = false,
        interpret = nil,
        title = nil
    }

    return setmetatable(self, { __index = ShelfPlace })
end

-- CD einstellen
function ShelfPlace:put(title, interpret)
    if self.in_use then return nil, 'already taken' end

    self.in_use = true
    self.interpret = interpret
    self.title = title

    return true
end

-- CD entfernen
function ShelfPlace:take()
    if not self.in_use then return nil, 'empty' end

    local interpret = self.interpret
    local title = self.title

    self.in_use = false
    self.interpret = nil
    self.title = nil

    return title, interpret
end

-- Titel und Interpret zurück geben
function ShelfPlace:show()
    return self.title, self.interpret
end


-- Regal Klasse
local Shelf = {}

-- Konstruktur
function Shelf.new()
    local self = {
        places = {}
    }

    -- 100 Regalplätze erstellen
    for i = 1, 100 do
        self.places[i] = ShelfPlace.new()
    end

    return setmetatable(self, { __index = Shelf })
end

-- CD an Position im Regal stellen
function Shelf:insert_cd(title, interpret, pos)
    pos = tonumber(pos)
    if not title or not interpret or not pos then
        return nil, 'title, interpret and position needed'
    end

    local place = self.places[pos]
    local ok, err = place:put(title, interpret)

    return ok, err
end

-- CD von Position im Regal entfernen
function Shelf:remove_cd(pos)
    pos = tonumber(pos)
    if not pos then
        return nil, 'position needed'
    end

    local place = self.places[pos]
    local ok, err = place:take()

    return ok, err
end

-- erstelle compare Funktion abhängig nur Titel oder Titel und Interpret
-- übergeben wurde
function Shelf:_search_comparee(title, interpret)
    title = title:lower()

    if title and interpret then
        interpret = interpret:lower()

        return function(t, i)
            return t:lower() == title and i:lower() == interpret
        end
    elseif title then
        return function(t)
            return t:lower() == title
        end
    end
end

-- Suche CD nach Titel oder Titel und Interpret
function Shelf:search(title, interpret)
    if not title then
        return nil, 'title needed'
    end

    local compare_fn = self:_search_comparee(title, interpret)

    for idx, place in ipairs(self.places) do
        if place.in_use then
            local t, i = place:show()

            if compare_fn(t, i) then
                return idx
            end
        end
    end

    return 0
end

-- Regal eintrag ausgeben
function Shelf:_print_shelf_place(nr, title, interpret)
    io.write(string.format(' %-4d | %-15s | %-15s\n', nr, title, interpret))
end

-- Alle Titel eines Interpreter im Regal ausgeben
function Shelf:show_all_for_interpret(interpret)
    if not interpret then
        io.write('ERROR: interpret needed\n')
        return
    end

    interpret = interpret:lower()

    for idx, place in ipairs(self.places) do
        if place.in_use then
            local t, i = place:show()

            if i:lower() == interpret then
                self:_print_shelf_place(idx, t, i)
            end
        end
    end
end

-- Zeige was im Regal steht
function Shelf:show_all()
    for idx, place in ipairs(self.places) do
        if place.in_use then
            local t, i = place:show()
            self:_print_shelf_place(idx, t, i)
        end
    end
end

-- Helper split Funktion
local function split(str, sep)
    local splitted = {}
    sep = sep or ' '

    for s in str:gmatch('([^' .. sep .. ']+)') do
        table.insert(splitted, s)
    end

    return splitted
end

-- Main Funktion run
local function run()
    local stopped = false
    local shelf = Shelf.new()

    while not stopped do
        io.write('> ')
        local input = split(io.read())
        local cmd = table.remove(input, 1)

        if cmd == 'q' or cmd == 'quit' or cmd == 'exit' then
            stopped = true
        elseif cmd == 'add' then
            local ok, err = shelf:insert_cd(table.unpack(input))
            if ok then
                io.write('OK\n')
            else
                io.write(string.format('ERROR: %s\n', err))
            end
        elseif cmd == 'del' then
            local ok, err = shelf:remove_cd(table.unpack(input))
            if ok then
                io.write('OK\n')
            else
                io.write(string.format('ERROR: %s\n', err))
            end
        elseif cmd == 'search' then
            local ok, err = shelf:search(table.unpack(input))
            if ok then
                io.write(ok .. '\n')
            else
                io.write(string.format('ERROR: %s\n', err))
            end
        elseif cmd == 'show-all' then
            shelf:show_all()
        elseif cmd == 'show-from-interpret' then
            shelf:show_all_for_interpret(table.unpack(input))
        else
            io.write('Unknown command\n')
        end
    end
end

run()
